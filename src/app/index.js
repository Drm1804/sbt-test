'use strict';
angular.module('sbtTest', ['ui.router']);


// Записываем в константу данные, которые будем использовать в разных частях приложения
angular.module('sbtTest')
  .constant('$configApp',  {
    defaultState: 'directories',
    api: {
      url: 'serv'
    }
  });




angular.module('sbtTest')
  .config(function($stateProvider, $urlRouterProvider, $directoriesProvider, $directoryProvider, $entryProvider, $configApp){
    $urlRouterProvider.otherwise('/directories');



    // Конфигурируем провайдеры. Если наш сервер передет на новый адресс, то придется поменять его т олкьо в одом месте
    $directoriesProvider.configure({
      apiUrl: $configApp.api.url
    });

    $directoryProvider.configure({
      apiUrl: $configApp.api.url
    });

    $directoryProvider.configure({
      apiUrl: $configApp.api.url
    });

    $entryProvider.configure({
      apiUrl: $configApp.api.url
    });
  });
