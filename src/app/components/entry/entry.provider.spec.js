'use strict';

describe('Тестируем провайдер $entry', function(){
  var $entryProvider;
  beforeEach(module('sbtTest', function(_$entryProvider_){
    $entryProvider = _$entryProvider_;
  }));

  var $entry, $httpBackend;

  beforeEach(inject(function (_$entry_, _$httpBackend_) {
    $entry = _$entry_;
    $httpBackend = _$httpBackend_;
  }));

  it('Должен вернуть true, значит провайдер существует', function () {
    expect($entryProvider).not.toBeUndefined();
  });

  describe('Тестируем метод configure', function () {

    it('Должен вернуть исключение, если передали не объект', function () {
      var data = {};

      expect(function () {
        $entryProvider.configure(data);
      }).not.toThrow();
    });

    it('Должен вернуть объект', function () {
      var data = {};

      expect(angular.isObject($entryProvider.configure(data))).toBe(true);
    });

  });

  describe('Тестирум методы провайдера $directory', function () {

    describe('Тестируем метод returnEntryData', function () {

      afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('Проверяем запрос к серверу', function() {
        $httpBackend.expectGET('serv/getEntry.json').respond('');
        $entry.returnEntryData();
        $httpBackend.flush();
      });

      it('Проверяем корректноть обработки полученных данных', function () {
        var resp = {data: ['one thing', 'another thing']};
        var myData = [];
        var errorStatus = '';
        var handler = {
          success: function(data) {
            myData = data;
          },
          error: function(data) {
            errorStatus = data;
          }
        };

        spyOn(handler, 'success').and.callThrough();
        spyOn(handler, 'error').and.callThrough();

        $httpBackend.expectGET('serv/getEntry.json').respond(resp.data);
        $entry.returnEntryData().then(handler.success, handler.error);
        $httpBackend.flush();

        expect(handler.success).toHaveBeenCalled();
        expect(myData).toEqual(resp.data);
        expect(handler.error).not.toHaveBeenCalled();
        expect(errorStatus).toEqual('');

      });

    });

  });
});
