'use strict';

describe('Тестирум контроллер EntryController ', function(){

  beforeEach(module('sbtTest'));

  var controller, scope, dfd, $entry;

  // Мокаем сервис и шпионим за вызовами
  beforeEach(inject(function ($q, _$entry_) {
    dfd = $q.defer();
    $entry = _$entry_;
    spyOn($entry, 'returnEntryData').and.returnValue(dfd.promise);
  }));

  // Инжектим контроллер и создаем $scope
  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    controller = $controller('EntryController', {
      $scope: scope
    });
  }));

  it('Контроллер должен существовать', function () {
    expect(controller).toBeDefined();
  });


  describe('Тестируем метод getData', function () {
    it("Метод returnEntryData в провайдере $entry должен быть вызван", function () {
      expect($entry.returnEntryData).toHaveBeenCalled();

    });

  });



});
