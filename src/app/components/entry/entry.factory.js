'use strict';

angular.module('sbtTest')
  .factory('entryFactory', function () {
    return{
      stringToDate: function(string){

        var date = new Date(string);

        return date;
      }

    }
  });
