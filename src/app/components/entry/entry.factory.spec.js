'use strict';

describe('Тестируем фабрику entryFactory', function(){

  beforeEach(module('sbtTest'));

  var entryFactory;

  beforeEach(inject(function (_entryFactory_) {
    entryFactory = _entryFactory_;
  }));

  it("Фабрика должна существовать", function () {
    expect(entryFactory).toBeDefined();
  });

  describe("Тестируем методы фабрики", function () {

    it("Фабрика должна вернуть тип дата", function () {
      var string = '12.02.2015';

      var data = entryFactory.stringToDate(string);

      expect(angular.isDate(data)).toBe(true)
    });

  });

});
