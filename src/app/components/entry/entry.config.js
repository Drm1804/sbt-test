'use strict';

angular.module('sbtTest')
  .config(function ($stateProvider) {
    $stateProvider
      .state('entry', {
        url: '/entry',
        templateUrl: 'app/components/entry/entry.html'
      });
  });
