'use strict';

angular.module('sbtTest')
  .controller('EntryController', function ($scope, $entry, entryFactory) {
    $scope.entryDate = {};

    $scope.parent = [{
      link: 'directories',
      name: "Справочники"
    },{
        link: 'directory',
        name: "Типы ошибок"
      }];

    $scope.select = '';


    $scope.getData = function () {
      $entry.returnEntryData()
        .then(
          function (resp) {
            $scope.entryDate = resp;

            $scope.convertData();

          },
          function (resp) {

            // Если запрос не успешен, то возбуждаем ошибку

            throw resp;
          }
        )
    };

    $scope.getData();

    $scope.convertData = function(){
      var data = $scope.entryDate.response.entry.items;

      // Использую цикл фор, толкьо потому что тесты почему-то ругаются на forEach, а вообще он мне нравится больше
      for(var item in data){
        if(data[item].type == "DATE"){
          data[item].value = entryFactory.stringToDate(data[item].value);
        }
        else if(data[item].type == "SELECT"){
          var selectArr = data[item].values;

          for (var i in selectArr){
            if(selectArr[i].selected){
              $scope.select = selectArr[i].id
            }
          }
        }
      }

    }
  });
