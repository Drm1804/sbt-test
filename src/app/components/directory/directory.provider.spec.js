'use strict';

describe('Тестируем провайдер $directory', function(){

  var $directoryProvider;
  beforeEach(module('sbtTest', function(_$directoryProvider_){
    $directoryProvider = _$directoryProvider_;
  }));


  var $directory, $httpBackend;

  beforeEach(inject(function (_$directory_, _$httpBackend_) {
    $directory = _$directory_;
    $httpBackend = _$httpBackend_;
  }));

  it('Должен вернуть true, значит провайдер существует', function () {
    expect($directoryProvider).not.toBeUndefined();
  });

  describe('Тестируем метод configure', function () {

    it('Должен вернуть исключение, если передали не объект', function () {
      var data = {};

      expect(function () {
        $directoryProvider.configure(data);
      }).not.toThrow();
    });

    it('Должен вернуть объект', function () {
      var data = {};

      expect(angular.isObject($directoryProvider.configure(data))).toBe(true);
    });

  });

  describe('Тестирум методы провайдера $directory', function () {

    describe('Тестируем метод returnDirectoriesData', function () {

      afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('Проверяем запрос к серверу', function() {
        $httpBackend.expectGET('serv/getDirectory.json').respond('');
        $directory.returnDirectoryData();
        $httpBackend.flush();
      });

      it('Проверяем корректноть обработки полученных данных', function () {
        var resp = {data: ['one thing', 'another thing']};
        var myData = [];
        var errorStatus = '';
        var handler = {
          success: function(data) {
            myData = data;
          },
          error: function(data) {
            errorStatus = data;
          }
        };

        spyOn(handler, 'success').and.callThrough();
        spyOn(handler, 'error').and.callThrough();

        $httpBackend.expectGET('serv/getDirectory.json').respond(resp.data);
        $directory.returnDirectoryData().then(handler.success, handler.error);
        $httpBackend.flush();

        expect(handler.success).toHaveBeenCalled();
        expect(myData).toEqual(resp.data);
        expect(handler.error).not.toHaveBeenCalled();
        expect(errorStatus).toEqual('');

      });

    });

  });
});
