'use strict';

angular.module('sbtTest')
  .provider('$directory', function(){

    var configs = {
      apiUrl: '/api/',
      directory: 'getDirectory.json'
    };

    return{

      configure: function(params){
          if (params instanceof Object) {
            angular.extend(configs, params);
          } else {
            throw ("Argument exception: config params should be an Object");
          }
          return configs;
      },
      $get: function($http, $q){
        return{
          returnDirectoryData: function(){

            // Поскольку предполагается, что данные находятся на сервер, запрос делаем асинхронно. Используем промисы из библиотеки $q
            var dfd = $q.defer();

            // Делаем запрос
            $http.get(configs.apiUrl +'/'+ configs.directory)
              .then(
                function(resp){
                  dfd.resolve(resp.data);
                },
                function(resp){
                  dfd.reject(resp);
                }
              );

            return dfd.promise;
          }
        }
      }
    }


  });

