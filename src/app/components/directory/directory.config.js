'use strict';

angular.module('sbtTest')
  .config(function ($stateProvider) {
    $stateProvider
      .state('directory', {
        url: '/directory',
        templateUrl: 'app/components/directory/directory.html'
      });
  });
