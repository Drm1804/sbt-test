'use strict';

angular.module('sbtTest')
  .controller('DirectoryController', function ($scope, $directory) {
    $scope.dirDate = {};

    $scope.parent = [{
      link: 'directories',
      name: "Справочники"
    }];

    $scope.getData = function () {
      $directory.returnDirectoryData()
        .then(
          function (resp) {
            $scope.dirDate = resp;

            console.log( $scope.dirDate)
          },
          function (resp) {

            // Если запрос не успешен, то возбуждаем ошибку

            throw resp;
          }
        )
    };

    $scope.getData();
  });
