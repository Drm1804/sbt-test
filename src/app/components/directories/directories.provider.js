'use strict';

angular.module('sbtTest')
  .provider('$directories', function(){

    var configs = {
      apiUrl: '/api/',
      directories: 'getDirectories.json'
    };

    return{

      configure: function(params){
          if (params instanceof Object) {
            angular.extend(configs, params);
          } else {
            throw ("Argument exception: config params should be an Object");
          }
          return configs;
      },
      $get: function($http, $q){
        return{
          returnDirectoriesData: function(){

            // Поскольку предполагается, что данные находятся на сервер, запрос делаем асинхронно. Используем промисы из библиотеки $q
            var dfd = $q.defer();

            // Делаем запрос
            $http.get(configs.apiUrl +'/'+ configs.directories)
              .then(
                function(resp){
                  dfd.resolve(resp.data);
                },
                function(resp){
                  dfd.reject(resp);
                }
              );

            return dfd.promise;
          }
        }
      }
    }


  });

