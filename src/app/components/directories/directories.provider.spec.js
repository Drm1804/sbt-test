'use strict';

describe('Тестируем провайдер $directories', function(){

  var $directoriesProvider;
  beforeEach(module('sbtTest', function(_$directoriesProvider_){
    $directoriesProvider = _$directoriesProvider_;
  }));


  var $directories, $httpBackend;

  beforeEach(inject(function (_$directories_, _$httpBackend_) {
    $directories = _$directories_;
    $httpBackend = _$httpBackend_;
  }));

  it('Должен вернуть true, значит провайдер существует', function () {
    expect($directoriesProvider).not.toBeUndefined();
  });

  describe('Тестируем метод configure', function () {

    it('Должен вернуть исключение, если передали не объект', function () {
      var data = {};

      expect(function () {
        $directoriesProvider.configure(data);
      }).not.toThrow();
    });

    it('Должен вернуть объект', function () {
      var data = {};

      expect(angular.isObject($directoriesProvider.configure(data))).toBe(true);
    });

  });

  describe('Тестирум методы провайдера $directories', function () {

    describe('Тестируем метод returnDirectoriesData', function () {

      afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('Проверяем запрос к серверу', function() {
        $httpBackend.expectGET('serv/getDirectories.json').respond('');
        $directories.returnDirectoriesData();
        $httpBackend.flush();
      });

      it('Проверяем корректноть обработки полученных данных', function () {
        var resp = {data: ['one thing', 'another thing']};
        var myData = [];
        var errorStatus = '';
        var handler = {
          success: function(data) {
            myData = data;
          },
          error: function(data) {
            errorStatus = data;
          }
        };

        spyOn(handler, 'success').and.callThrough();
        spyOn(handler, 'error').and.callThrough();

        $httpBackend.expectGET('serv/getDirectories.json').respond(resp.data);
        $directories.returnDirectoriesData().then(handler.success, handler.error);
        $httpBackend.flush();

        expect(handler.success).toHaveBeenCalled();
        expect(myData).toEqual(resp.data);
        expect(handler.error).not.toHaveBeenCalled();
        expect(errorStatus).toEqual('');

      });

    });

  });
});
