'use strict';

angular.module('sbtTest')
  .config(function ($stateProvider) {
    $stateProvider
      .state('directories', {
        url: '/directories',
        templateUrl: 'app/components/directories/directories.html'
      });
  });
