'use strict';

describe('Тестирум контроллер DirectoriesController ', function(){

  beforeEach(module('sbtTest'));

  var controller, scope, dfd, $directories;

  // Мокаем сервис и шпионим за вызовами
  beforeEach(inject(function ($q, _$directories_) {
    dfd = $q.defer();
    $directories = _$directories_;
    spyOn($directories, 'returnDirectoriesData').and.returnValue(dfd.promise);
  }));

  // Инжектим контроллер и создаем $scope
  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    controller = $controller('DirectoriesController', {
      $scope: scope
    });
  }));

  it('Провайдер должен существовать', function () {
    expect(controller).toBeDefined();
  });


  describe('Тестируем метод getData', function () {
    it("Метод returnDirectoriesData в провайдере $directories должен быть вызван", function () {
      expect($directories.returnDirectoriesData).toHaveBeenCalled();

    });

    it('Возвращаем какие-то данные от сервера, и проверяем, чтобы они правильно записались', function () {
      var resp = ['something', 'on', 'success'];
      dfd.resolve(resp);
      scope.$digest();
      expect(scope.dirDate).toEqual(resp)
    });

  });



});
