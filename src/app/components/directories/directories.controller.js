'use strict';

angular.module('sbtTest')
  .controller('DirectoriesController', function ($scope, $directories) {
    $scope.dirDate = {};

    $scope.getData = function () {
      $directories.returnDirectoriesData()
        .then(
          function (resp) {
            $scope.dirDate = resp;
          },
          function (resp) {

            // Если запрос не успешен, то возбуждаем ошибку

            throw resp;
          }
        )
    };

    $scope.getData();
  });
